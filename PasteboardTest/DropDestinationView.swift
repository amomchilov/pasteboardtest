//
//  DropDestinationView.swift
//  PasteboardTest
//
//  Created by Alexander Momchilov on 2018-01-30.
//  Copyright © 2018 Alexander Momchilov. All rights reserved.
//

import AppKit

extension NSPasteboard.PasteboardType {
	static let all: [NSPasteboard.PasteboardType] = [
		.URL,
		.color,
		.fileContents,
		.filePromise,
		.fileURL,
		.findPanelSearchOptions,
		.font,
		.html,
		.inkText,
		.multipleTextSelection,
		.pdf,
		.png,
		.postScript,
		.rtf,
		.rtfd,
		.ruler,
		.sound,
		.string,
		.tabularText,
		.textFinderOptions,
		.tiff,
		.vCard
	]
}

protocol DropDestinationViewDelegate {
	func concludeDragOperation(_ dragInfo: NSDraggingInfo)
}

class DropDestinationView: NSBox {
	
	var delegate: DropDestinationViewDelegate?

	required init?(coder decoder: NSCoder) {
		super.init(coder: decoder)
		self.registerForDraggedTypes(NSPasteboard.PasteboardType.all)
	}
}

// NSDraggingDestination
extension DropDestinationView {
	override func draggingEntered(_ sender: NSDraggingInfo) -> NSDragOperation {
		return .copy
	}
	
	override func performDragOperation(_ sender: NSDraggingInfo) -> Bool {
		return true
	}
	
	override func concludeDragOperation(_ sender: NSDraggingInfo?) {
		guard let dragInfo = sender else { return }
		delegate?.concludeDragOperation(dragInfo)
	}
}
