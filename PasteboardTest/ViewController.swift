//
//  ViewController.swift
//  PasteboardTest
//
//  Created by Alexander Momchilov on 2018-01-30.
//  Copyright © 2018 Alexander Momchilov. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {

	@IBOutlet var dropDestinationView: DropDestinationView!
	
	@IBOutlet var outlineView: NSOutlineView!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		dropDestinationView.delegate = self
	}

	override var representedObject: Any? {
		didSet {
			outlineView.reloadData()
		}
	}

	var representedDrag: DraggingInfoModel? {
		get { return self.representedObject as? DraggingInfoModel }
		set { self.representedObject = newValue }
	}
}

extension ViewController: DropDestinationViewDelegate {
	func concludeDragOperation(_ dragInfo: NSDraggingInfo) {
		self.representedDrag = DraggingInfoModel(dragInfo)
	}
}



extension ViewController: NSOutlineViewDelegate {
	
	func outlineView(_ outlineView: NSOutlineView, viewFor tableColumn: NSTableColumn?, item: Any) -> NSView? {
		guard let child = item as? Child else {
			fatalError("Trying to display an Item that is not a chlid")
		}
		
		func makeCell(stringValue: String) -> NSTableCellView {
			let view = outlineView.makeView(
				withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "Cell"),
				owner: self
				) as! NSTableCellView
			
			view.textField!.stringValue = stringValue
			return view
		}
		
		switch tableColumn?.identifier.rawValue {
		case "Label"?: return makeCell(stringValue: child.label)
		case "Value"?: return makeCell(stringValue: child.value)
		case _: return nil
		}
	}
}

extension ViewController: NSOutlineViewDataSource {
	private func convertToParent(_ item: Any?) -> Parent {
		switch item {
		case nil: return self.representedDrag ?? EmptyParent.shared
		case let p as Parent: return p
		case _: fatalError("Item is not a parent.")
		}
	}
	
	func outlineView(_ outlineView: NSOutlineView, numberOfChildrenOfItem item: Any?) -> Int {
		return convertToParent(item).children.count
	}
	
	func outlineView(_ outlineView: NSOutlineView, child index: Int, ofItem item: Any?) -> Any {
		let child = convertToParent(item).children[index]
		
		if let childAsParent = child as? Parent {
			DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(1)) { // TODO: gross
				if childAsParent.expandChildrenByDefault { outlineView.expandItem(childAsParent) }
			}
		}
		
		return child
	}
	
	func outlineView(_ outlineView: NSOutlineView, isItemExpandable item: Any) -> Bool {
		return item is Parent
	}
}

