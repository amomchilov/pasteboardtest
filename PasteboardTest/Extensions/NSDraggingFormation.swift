//
//  NSDraggingFormation.swift
//  PasteboardTest
//
//  Created by Alexander Momchilov on 2018-01-30.
//  Copyright © 2018 Alexander Momchilov. All rights reserved.
//

import AppKit

extension NSDraggingFormation: CustomDebugStringConvertible {
	public var debugDescription: String {
		switch self {
		case .default: return "NSDraggingFormation.default"
		case .none: return "NSDraggingFormation.none"
		case .pile: return "NSDraggingFormation.pile"
		case .list: return "NSDraggingFormation.list"
		case .stack: return "NSDraggingFormation.stack"
		}
	}
}
