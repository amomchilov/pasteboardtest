//
//  NSSpringLoadingHighlight.swift
//  PasteboardTest
//
//  Created by Alexander Momchilov on 2018-01-30.
//  Copyright © 2018 Alexander Momchilov. All rights reserved.
//

import AppKit

extension NSSpringLoadingHighlight: CustomDebugStringConvertible {
	public var debugDescription: String {
		switch self {
		case .none: return "NSSpringLoadingHighlight.none"
		case .standard: return "NSSpringLoadingHighlight.standard"
		case .emphasized: return "NSSpringLoadingHighlight.emphasized"
		}
	}
}
