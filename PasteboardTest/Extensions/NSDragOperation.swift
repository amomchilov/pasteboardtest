//
//  NSDragOperation.swift
//  PasteboardTest
//
//  Created by Alexander Momchilov on 2018-01-30.
//  Copyright © 2018 Alexander Momchilov. All rights reserved.
//

import AppKit

extension NSDragOperation: CustomDebugStringConvertible {
	public var debugDescription: String {
		if self == [] { return "[]" }
		if self.contains(.every) { return "[.every]" }
		
		let middle = [
			self.contains(.copy)	? ".copy"		: nil,
			self.contains(.link)	? ".link"		: nil,
			self.contains(.generic) ? ".generic"	: nil,
			self.contains(.private) ? ".private"	: nil,
			self.contains(.move)	? ".move"		: nil,
			self.contains(.delete)	? ".delete"		: nil,
		]
			.flatMap { $0 }
			.joined(separator: ", ")
		
		return "[\(middle)]"
	}
}
