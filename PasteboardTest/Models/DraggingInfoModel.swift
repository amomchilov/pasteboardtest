//
//  DraggingInfoModel.swift
//  PasteboardTest
//
//  Created by Alexander Momchilov on 2018-01-30.
//  Copyright © 2018 Alexander Momchilov. All rights reserved.
//

import AppKit

class DraggingInfoModel: Parent {
	let pasteboard: NSPasteboard
	let sequenceNumber: Int
	let source: Any?
	let sourceOperationMask: NSDragOperation
	let destWindow: NSWindow?
	let numberOfValidItemsForDrop: Int
	let image: NSImage?
	let draggedImageLocation: NSPoint?
	let animatesToDestination: Bool
	let draggingFormation: NSDraggingFormation
	let springLoadingHighlight: NSSpringLoadingHighlight
	
	let children: [Child]
	let expandChildrenByDefault = true
	
	init(_ dragInfo: NSDraggingInfo) {
		
		self.pasteboard					= dragInfo.draggingPasteboard()
		self.sequenceNumber				= dragInfo.draggingSequenceNumber()
		self.source						= dragInfo.draggingSource()
		self.sourceOperationMask		= dragInfo.draggingSourceOperationMask()
		self.destWindow					= dragInfo.draggingDestinationWindow()
		self.numberOfValidItemsForDrop	= dragInfo.numberOfValidItemsForDrop
		self.image						= dragInfo.draggedImage()
		self.draggedImageLocation		= dragInfo.draggedImageLocation()
		//		_ = dragInfo.slideDraggedImage(to: NSPoint())
		self.animatesToDestination		= dragInfo.animatesToDestination
		self.draggingFormation			= dragInfo.draggingFormation
		//		_ = dragInfo.enumerateDraggingItems(options: [], for: self, classes: <#T##[AnyClass]#>, searchOptions: <#T##[NSPasteboard.ReadingOptionKey : Any]#>, using: <#T##(NSDraggingItem, Int, UnsafeMutablePointer<ObjCBool>) -> Void#>)
		self.springLoadingHighlight	= dragInfo.springLoadingHighlight
		
		self.children = [
			PasteboardModel(pasteboard),
			LeafNode(label: "Sequence Number",			value: sequenceNumber),
			LeafNode(label: "Source",					value: source as Any),
			LeafNode(label: "Source Operation Mask", 	value: sourceOperationMask.debugDescription),
			LeafNode(label: "Destination Window",		value: destWindow),
			LeafNode(label: "Number of Valid Items",	value: numberOfValidItemsForDrop),
			LeafNode(label: "Dragged Image",			value: image),
			LeafNode(label: "Dragged Image Location",	value: draggedImageLocation),
			LeafNode(label: "Animates to destination",	value: animatesToDestination),
			LeafNode(label: "Dragging formation",		value: draggingFormation),
			LeafNode(label: "Spring Loading Highlight",	value: springLoadingHighlight)
		]
	}
}
