//
//  PasteboardModel.swift
//  PasteboardTest
//
//  Created by Alexander Momchilov on 2018-01-30.
//  Copyright © 2018 Alexander Momchilov. All rights reserved.
//

import AppKit

class PasteboardModel: Parent, Child {
	let pasteboard: NSPasteboard
	
	let label = "Pasteboard"
	let value: String
	
	let children: [Child]
	let expandChildrenByDefault = true
	
	init(_ pasteboard: NSPasteboard) {
		self.pasteboard = pasteboard
		self.value = String(describing: pasteboard)
		
		let pasteboardItems = pasteboard.pasteboardItems ?? []
		
		self.children = [
			LeafNode(label: "Name", value: pasteboard.name.rawValue),
			LeafNode(label: "Change Count", value: pasteboard.changeCount),
			PasteboardItemsModel(pasteboardItems)
		]
	}
	
}
