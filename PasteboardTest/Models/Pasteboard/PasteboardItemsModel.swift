//
//  PasteboardItemsModel.swift
//  PasteboardTest
//
//  Created by Alexander Momchilov on 2018-02-02.
//  Copyright © 2018 Alexander Momchilov. All rights reserved.
//

import AppKit

class PasteboardItemsModel: Node {
	let items: [NSPasteboardItem]
	
	let label = "pasteboardItems"
	let value = ""
	let children: [Child]
	let expandChildrenByDefault = true
	
	init(_ items: [NSPasteboardItem]) {
		self.items = items
		
		self.children = items.map(PasteboardItemModel.init)
	}
}
