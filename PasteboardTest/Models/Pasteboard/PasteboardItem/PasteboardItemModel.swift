//
//  PasteboardItemModel.swift
//  PasteboardTest
//
//  Created by Alexander Momchilov on 2018-01-30.
//  Copyright © 2018 Alexander Momchilov. All rights reserved.
//

import AppKit

class PasteboardItemModel: Node {
	let item: NSPasteboardItem
	
	let label = "NSPasteboardItem"
	let value: String
	let children: [Child]
	let expandChildrenByDefault = true
	
	init(_ item: NSPasteboardItem) {
		self.item = item
		self.value = String(describing: item)
		
		self.children = item.types.map{ PasteboardItemTypeModel($0, parentPasteboardItem: item) }
	}
}
