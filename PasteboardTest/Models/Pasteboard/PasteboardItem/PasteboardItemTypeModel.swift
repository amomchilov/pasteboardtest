//
//  PasteboardItemTypeModel.swift
//  PasteboardTest
//
//  Created by Alexander Momchilov on 2018-01-30.
//  Copyright © 2018 Alexander Momchilov. All rights reserved.
//

import AppKit

class PasteboardItemTypeModel: Node {
	let type: NSPasteboard.PasteboardType
	let parentPasteboardItem: NSPasteboardItem
	
	let label: String
	let value: String
	let children: [Child]
	let expandChildrenByDefault = false
	
	init(_ type: NSPasteboard.PasteboardType, parentPasteboardItem: NSPasteboardItem) {
		self.type = type
		self.parentPasteboardItem = parentPasteboardItem
		
		self.label = PasteboardItemTypeModel.mapping[type] ?? "Unknown"
		self.value = type.rawValue
		
		self.children = [
			parentPasteboardItem.data(forType: type)		.map { LeafNode(label: "Data",	 		value: $0) } ?? nil,
			parentPasteboardItem.string(forType: type)		.map { LeafNode(label: "String", 		value: $0) } ?? nil,
			parentPasteboardItem.propertyList(forType: type).map { LeafNode(label: "Property List", value: $0) } ?? nil,
			
		].flatMap { $0 }
	}
	
	static let mapping: [NSPasteboard.PasteboardType: String] = [
		.URL: ".URL",
		.color: ".color",
		.fileContents: ".fileContents",
		.filePromise: ".filePromise",
		.fileURL: ".fileURL",
		.findPanelSearchOptions: ".findPanelSearchOptions",
		.font: ".font",
		.html: ".html",
		.inkText: ".inkText",
		.multipleTextSelection: ".multipleTextSelection",
		.pdf: ".pdf",
		.png: ".png",
		.postScript: ".postScript",
		.rtf: ".rtf",
		.rtfd: ".rtfd",
		.ruler: ".ruler",
		.sound: ".sound",
		.string: ".string",
		.tabularText: ".tabularText",
		.textFinderOptions: ".textFinderOptions",
		.tiff: ".tiff",
		.vCard: ".vCard",
	]
}
