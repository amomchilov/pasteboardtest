//
//  OutlineViewNode.swift
//  PasteboardTest
//
//  Created by Alexander Momchilov on 2018-01-30.
//  Copyright © 2018 Alexander Momchilov. All rights reserved.
//

protocol Parent {
	var children: [Child] { get }
	var expandChildrenByDefault: Bool { get }
}

extension Parent {
	var expandChildrenByDefault: Bool { return false }
}

protocol Child {
	var label: String { get }
	var value: String { get }
}

protocol Node: Child, Parent {}




class LeafNode: Child {
	let label: String
	let value: String
	
	init(label: String, value: String) {
		self.label = label
		self.value = value
	}
	
	convenience init<T: CustomDebugStringConvertible>(label: String, value: T?) {
		self.init(label: label, value: value.map { $0.debugDescription } ?? "nil")
	}
	
	convenience init(label: String, value: Any) {
		self.init(label: label, value: String(describing: value))
	}
}

class EmptyParent: Parent {
	static let shared = EmptyParent()
	private init() {}
	
	let children: [Child] = []
}
